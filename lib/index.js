"use strict";
// var restify = require('restify');
// var builder = require('botbuilder');
// var botemulatorUrl = "http://192.168.11.50:3978/api/message";
// // Create bot and add dialogs
// var bot = new builder.BotConnectorBot({ appId: 'YourAppId', appSecret: 'YourAppSecret' });
// bot.add('/', function (session) {
//     session.send('Hello man!');
//     session.beginDialog('/profile');
// });
//
// bot.add("/profile", [function(session){
//     builder.Prompts.text(session, "qui est tu?!");
// },function(session, results){
//     session.userData.name = results.response;
//     session.endDialog();
// }]);
//
// // Setup Restify Server
// var server = restify.createServer();
// server.post('/api/messages', bot.verifyBotFramework(), bot.listen());
// server.listen(process.env.port || 3978, function () {
//     console.log('%s listening to %s', server.name, server.url);
// });


// var apiAccount = "belisarus";
// var apiKey = "b2de0298804342018df4c20ae6815f92";
// var botUrl = "https://api.projectoxford.ai/luis/v1/application?id=b62ae66d-b177-47ad-9f02-9612e04b614b&subscription-key=10809dd2295142209661b6984c392599";
var botUrl = "https://api.projectoxford.ai/luis/v1/application?id=db30fe8b-0556-4b3b-b330-92938795a024&subscription-key=10809dd2295142209661b6984c392599";


var builder = require('botbuilder');

var helloBot = new builder.TextBot();
helloBot.add('/', new builder.CommandDialog()
.matches('^set name', builder.DialogAction.beginDialog('/profile'))
.matches('^travel', builder.DialogAction.beginDialog('/travel'))
.matches('^quit', builder.DialogAction.endDialog())
.onDefault(function (session) {
  if (!session.userData.name) {
    session.beginDialog('/profile');
  } else {
    session.send('Hello %s!', session.userData.name);
  }
}));
helloBot.add('/profile',  [
  function (session) {
    if (session.userData.name) {
      builder.Prompts.text(session, 'What would you like to change it to?');
    } else {
      builder.Prompts.text(session, 'Hi! What is your name?');
    }
  },
  function (session, results) {
    session.userData.name = results.response;
    session.endDialog();
  }
]);

var luisDialog = new builder.LuisDialog(botUrl);

luisDialog.onBegin(function(session, args, next){
  session.send("ou veux tu aller?");
});


luisDialog.on("Destination", [function(session, args, next){
  var to = builder.EntityRecognizer.findEntity(args.entities, "Location::ToLocation");
  session.dialogData.trip = {to: to.entity};

  // builder.Prompts.text(session, 'Quand veux tu partir?');
  builder.Prompts.text(session, "J'ai 4 annonces à disponibles sur " + session.dialogData.trip.to + ": \n annonce 1,\n annonce 2,\n annonce 3,\n annonce 4");
},
function(session, result, next){
  next();
}]);

helloBot.add("/travel", luisDialog);
luisDialog.onDefault(function(session){
  builder.DialogAction.send("Je ne sais pas!");
});
helloBot.listenStdin();